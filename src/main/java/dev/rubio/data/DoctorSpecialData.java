package dev.rubio.data;

import dev.rubio.models.DoctorSpecial;

import java.util.ArrayList;
import java.util.List;

public class DoctorSpecialData {
    private List<DoctorSpecial> doctors = new ArrayList<>();

    public DoctorSpecialData(){

        doctors.add(new DoctorSpecial(45671,"ADHD, Depression", "Anthem", "Jennifer Rose"));
    }
}
