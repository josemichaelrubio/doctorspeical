package dev.rubio;

import io.javalin.Javalin;
import io.javalin.http.Context;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


public class Driver {
  private static Logger logger= LogManager.getLogger();

    public static void main(String[] args) {
        //How to use Javalin
        Javalin app = Javalin.create().start(8000); //port is automatically 7000 if nothing is there

        app.get("/",(Context ctx) ->{ctx.result("Hello World");});
        //

        logger.trace("logging debug message");
        logger.debug("logging debug message");
        logger.info("logging ingo message");
        logger.warn("logging warn message ");
        logger.error("logging error message");
        logger.fatal("Logging Fatal Message");

        Driver d = new Driver();
    }
}
