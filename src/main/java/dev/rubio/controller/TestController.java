package dev.rubio.controller;

import io.javalin.http.Context;
import org.apache.logging.log4j.core.config.plugins.util.ResolverUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class TestController {
    Logger logger = LoggerFactory.getLogger(TestController.class);
    TestController test = new TestController();
    public void handleGetItemsRequest(Context ctx){
        logger.info("Getting all items");
        ctx.json(test.();)
    }
}
